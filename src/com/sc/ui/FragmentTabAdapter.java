package com.sc.ui;

import java.util.List;

import com.sc.lane.R;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RadioButton;

public class FragmentTabAdapter implements OnClickListener{
	private List<Fragment> fragments;
	private FragmentActivity fragmentActivity;
	private int currentTab;
	private RadioButton mrbLocation;
	private RadioButton mrbRecord;
	private RadioButton mrbTrack;
	private int fragmentContentId;

	public FragmentTabAdapter(FragmentActivity fragmentActivity, List<Fragment> fragments, int fragmentContentId,
			RadioButton location, RadioButton record, RadioButton track) {
        this.fragments = fragments;
        this.fragmentActivity = fragmentActivity;
        this.fragmentContentId = fragmentContentId;
        
        mrbLocation = location;
        mrbLocation.setOnClickListener(this);
        mrbLocation.setChecked(true);
        mrbRecord = record;
        mrbRecord.setOnClickListener(this);
        mrbRecord.setChecked(false);
        mrbTrack = track;
        mrbTrack.setOnClickListener(this);
        mrbTrack.setChecked(false);

        // 默认显示第一页
        FragmentTransaction ft = fragmentActivity.getSupportFragmentManager().beginTransaction();
        ft.add(fragmentContentId, fragments.get(0));
        ft.commit();
    }
	
    public void onCheckedChanged(int checkedId) {
        Fragment fragment = fragments.get(checkedId);
        FragmentTransaction ft = obtainFragmentTransaction(checkedId);

//      getCurrentFragment().onPause(); // 暂停当前tab
//      getCurrentFragment().onStop(); // 暂停当前tab

        if(fragment.isAdded()){
//          fragment.onStart(); // 启动目标tab的onStart()
//          fragment.onResume(); // 启动目标tab的onResume()
        }else{
            ft.add(fragmentContentId, fragment);
        }
        showTab(checkedId); // 显示目标tab
        ft.commit();
    }
    
    private void showTab(int idx){
        for(int i = 0; i < fragments.size(); i++){
            Fragment fragment = fragments.get(i);
            FragmentTransaction ft = obtainFragmentTransaction(idx);

            if(idx == i){
                ft.show(fragment);
            }else{
                ft.hide(fragment);
            }
            ft.commit();
        }
        currentTab = idx; // 更新目标tab为当前tab
    }
    
    private FragmentTransaction obtainFragmentTransaction(int index){
        FragmentTransaction ft = fragmentActivity.getSupportFragmentManager().beginTransaction();
        return ft;
    }
    
    public int getCurrentTab() {
        return currentTab;
    }

    public Fragment getCurrentFragment(){
    	return fragments.get(currentTab);
    }

	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.main_footbar_loc :
			onCheckedChanged(0);
			showTab(0);
			mrbLocation.setChecked(true);
	        mrbRecord.setChecked(false);
	        mrbTrack.setChecked(false);
			break;
		case R.id.main_footbar_record :
			onCheckedChanged(1);
			showTab(1);
			mrbLocation.setChecked(false);
	        mrbRecord.setChecked(true);
	        mrbTrack.setChecked(false);
			break;
		case R.id.main_footbar_track :
			onCheckedChanged(2);
			showTab(2);
			mrbLocation.setChecked(false);
	        mrbRecord.setChecked(false);
	        mrbTrack.setChecked(true);
			break;
		case R.id.main_footbar_setting : 
			break;
		}
	}
}
