package com.sc.ui;

import greendroid.widget.MyQuickAction;
import greendroid.widget.QuickActionGrid;
import greendroid.widget.QuickActionWidget;
import greendroid.widget.QuickActionWidget.OnQuickActionClickListener;

import java.util.ArrayList;
import java.util.List;

import com.sc.common.UIHelper;
import com.sc.lane.R;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;

public class MainActivity extends FragmentActivity { 
	public static final int QUICKACTION_LOGIN_OR_LOGOUT = 0;
	public static final int QUICKACTION_USERINFO = 1;
	public static final int QUICKACTION_SETTING = 0;
	public static final int QUICKACTION_EXIT = 1;
	
	private List<Fragment> fragments = new ArrayList<Fragment>();
	private RadioButton mrbLocation;
	private RadioButton mrbRecord;
	private RadioButton mrbTrack;
	
	private QuickActionWidget mGrid;// 快捷栏控件
	private ImageView mivSetting;
	
    @Override  
    public void onCreate(Bundle savedInstanceState) {  
        super.onCreate(savedInstanceState);  
        setContentView(R.layout.main); 
        
        this.initQuickActionGrid();
        
        mrbLocation = (RadioButton)this.findViewById(R.id.main_footbar_loc);
        mrbRecord = (RadioButton)this.findViewById(R.id.main_footbar_record);
        mrbTrack = (RadioButton)this.findViewById(R.id.main_footbar_track);
        mivSetting = (ImageView) findViewById(R.id.main_footbar_setting);
		mivSetting.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				// 展示快捷栏&判断是否登录&是否加载文章图片
				//UIHelper.showSettingLoginOrLogout(MainActivity.this,
				//		mGrid.getQuickAction(0));
				mGrid.show(v);
			}
		});
  
        fragments.add(new FragementLocation());
        fragments.add(new FragementRecord());
        fragments.add(new FragementPhotos());
        
        FragmentTabAdapter tabAdapter = new FragmentTabAdapter(this, fragments, R.id.tab_content,
        		mrbLocation, mrbRecord, mrbTrack); 
    }

	private void initQuickActionGrid() {
		mGrid = new QuickActionGrid(this);
		/*
		mGrid.addQuickAction(new MyQuickAction(this, R.drawable.ic_menu_login,
				R.string.main_menu_login));
		mGrid.addQuickAction(new MyQuickAction(this, R.drawable.ic_menu_myinfo,
				R.string.main_menu_myinfo));
				*/
		mGrid.addQuickAction(new MyQuickAction(this, R.drawable.ic_menu_setting,
				R.string.main_menu_setting));
		mGrid.addQuickAction(new MyQuickAction(this, R.drawable.ic_menu_exit,
				R.string.main_menu_exit));
		mGrid.setOnQuickActionClickListener(mActionListener);
	}  
	
	/**
	 * 快捷栏item点击事件
	 */
	private OnQuickActionClickListener mActionListener = new OnQuickActionClickListener() {
		public void onQuickActionClicked(QuickActionWidget widget, int position) {
			switch (position) {
			/*
			case QUICKACTION_LOGIN_OR_LOGOUT:// 用户登录-注销

				break;
			case QUICKACTION_USERINFO:// 我的资料

				break;
				*/
			case QUICKACTION_SETTING:// 设置

				break;
			case QUICKACTION_EXIT:// 退出
				MainActivity.this.finish();
				break;
			}
		}
	};
}  
